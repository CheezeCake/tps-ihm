using System;
using System.Windows.Forms;
using System.Drawing;

public class Window : Form
{
	Button start;
	Button stop;
	Label label;

	private void clickStart(object sender, EventArgs e)
	{
		DialogResult ret = MessageBox.Show("Démarrer le processus ?", "Confirmation", MessageBoxButtons.YesNo);

		if(ret == DialogResult.Yes)
		{
			start.Enabled = false;
			stop.Enabled = true;
			label.Text = "Pressez arreter";
		}
	}

	private void clickStop(object sender, EventArgs e)
	{
		start.Enabled = true;
		stop.Enabled = false;
		label.Text = "Pressez Démarrer";
	}

	public Window()
	{
		start = new Button();
		start.Text = "Démarrer";
		start.Location = new Point(0, 0);
		start.Size = new Size(100, 30);
		start.Click += new EventHandler(clickStart);

		stop = new Button();
		stop.Text = "Arreter";
		stop.Location = new Point(110, 0);
		stop.Size = new Size(100, 30);
		stop.Enabled = false;
		stop.Click += new EventHandler(clickStop);

		label = new Label();
		label.Parent = this;
		label.Text = "Pressez Démarrer";
		label.Location = new Point(40, 50);
		label.Size = new Size(220, 70);

		Controls.Add(start);
		Controls.Add(stop);
		Size = new Size(210, 100);
	}

	public static void Main()
	{
		Application.Run(new Window());
	}
}
