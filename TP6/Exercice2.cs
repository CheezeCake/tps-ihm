using System;
using System.Windows.Forms;
using System.Drawing;

public class ConnectForm : Form
{
	TextBox mdpBox;
	TextBox idBox;
	CheckBox cb;

	private void afficherMdp(object sender, EventArgs e)
	{
		if(cb.Checked)
			mdpBox.PasswordChar = '*';
		else
			mdpBox.PasswordChar = '\0';
	}

	private void clean(object sender, EventArgs e)
	{
		mdpBox.ResetText();
		idBox.ResetText();
	}

	private void connection(object sender, EventArgs e)
	{
		if(idBox.Text == "")
			MessageBox.Show("Saisissez un nom d'utilisateur");
		else
			MessageBox.Show("L'utilisateur "+idBox.Text+" se connecte");
	}

	public ConnectForm()
	{
		Label id = new Label();
		id.Text = "Identifiant";
		id.Location = new Point(0, 0);
		id.Size = new Size(70, 30);

		idBox = new TextBox();
		idBox.Location = new Point(80, 0);
		idBox.Size = new Size(100, 30);

		Label mdp = new Label();
		mdp.Text = "Mot de Passe";
		mdp.Location = new Point(0, 30);
		mdp.Size = new Size(70, 30);

		mdpBox = new TextBox();
		mdpBox.Location = new Point(80, 30);
		mdpBox.Size = new Size(100, 30);

		cb = new CheckBox();
		cb.Location = new Point(90, 60);
		cb.Click += new EventHandler(afficherMdp);

		Label labelMdp = new Label();
		labelMdp.Parent = this;
		labelMdp.Text = "Afficher le mot de passe";
		labelMdp.Location = new Point(100, 60);
		labelMdp.Size = new Size(80, 30);

		Button con = new Button();
		con.Text = "Connecter";
		con.Location = new Point(20, 90);
		con.Size = new Size(65, 25);
		con.Click += new EventHandler(connection);

		Button an = new Button();
		an.Text = "Annuler";
		an.Location = new Point(100, 90);
		an.Size = new Size(65, 25);
		an.Click += new EventHandler(clean);

		Controls.Add(id);
		Controls.Add(idBox);
		Controls.Add(mdp);
		Controls.Add(mdpBox);
		Controls.Add(cb);
		Controls.Add(con);
		Controls.Add(an);
		Size = new Size(220, 150);
	}

	public static void Main()
	{
		ConnectForm c = new ConnectForm();
		c.StartPosition = FormStartPosition.CenterScreen;
		Application.Run(c);
	}
}
