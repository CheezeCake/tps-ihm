using System;

public class Test
{
	public static void Main()
	{
		Bouton b1 = new Bouton();
		Bouton b2 = new Bouton(Couleur.Vert);

		b1.affiche();
		b2.affiche();
		Console.WriteLine();

		ComposantGraphique[] tab = new ComposantGraphique[5];
		tab[0] = new Bouton();
		tab[1] = new Bouton(Couleur.Vert);
		tab[2] = new Menu();
		tab[3] = new Menu(Couleur.Vert);
		tab[4] = new Menu(Couleur.Blanc);

		for(int i = 0; i < tab.Length; i++)
		{
			if(tab[i] is Menu)
				((Menu)tab[i]).affiche();
			else
				tab[i].affiche();
		}

		Console.WriteLine();
		((Bouton)tab[0]).actif = false;

		for(int i = 0; i < tab.Length; i++)
		{
			if(tab[i] is Bouton && ((Bouton)tab[i]).actif)
				((Bouton)tab[i]).activer();
		}
	}
}
