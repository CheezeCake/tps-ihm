using System;

public enum Couleur { Noir = 0, Blanc = 1, Rouge = 2, Vert = 3, Bleu = 4 }

public class ComposantGraphique
{
	public Couleur couleur { get; set; }
	
	protected String identifiant;

	protected static int nbInstances = 0;

	public ComposantGraphique()
	{
		couleur = Couleur.Noir;
		nbInstances++;
	}

	public ComposantGraphique(Couleur couleur)
	{
		this.couleur = couleur;
		nbInstances++;
	}

	public virtual void affiche()
	{
		Console.WriteLine("Sa couleur est "+couleur);
	}
}
