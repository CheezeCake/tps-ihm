using System;

public class Bouton : ComposantActivable
{
	public Bouton() : base()
	{
		identifiant = "Bt "+nbInstances;
		actif = true;
	}

	public Bouton(Couleur couleur) : base(couleur)
	{
		identifiant = "Bt "+nbInstances;
		actif = true;
	}

	public override void affiche()
	{
		Console.WriteLine(identifiant);
		base.affiche();
	}

	public override void executerAction()
	{
		Console.WriteLine("Exécution de l'action du bouton "+identifiant);
	}
}
