using System;

public class Menu : ComposantGraphique
{
	public Menu() : base()
	{
		identifiant = "Mn "+nbInstances;
		couleur = Couleur.Bleu;
	}

	public Menu(Couleur couleur) : base(couleur)
	{
		identifiant = "Mn "+nbInstances;
	}

	public new void affiche()
	{
		Console.WriteLine(identifiant);
		base.affiche();
	}
}
