using System;

public abstract class ComposantActivable : ComposantGraphique
{
	public bool actif { get; set; }

	public ComposantActivable() : base()
	{
		actif = false;
	}

	public ComposantActivable(Couleur couleur) : base(couleur)
	{
		actif = false;
	}

	public void activer()
	{
		executerAction();
	}

	public abstract void executerAction();
}
