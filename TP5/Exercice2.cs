using System;
using System.Collections.Generic;

public class File<T>
{
	private List<T> liste;

	public File()
	{
		liste = new List<T>();
	}

	public void enfiler(T a)
	{
		liste.Add(a);
	}

	public T defiler()
	{
		int c = liste.Count;

		if(c > 0)
		{
			T ret = liste[c-1];
			liste.RemoveAt(c-1);
			return ret;
		}
		else
			throw new Exception("File vide");
	}

	public void afficher()
	{
		foreach(T element in liste)
			Console.Write(element+" ");
		Console.WriteLine();
	}
}

public class Test
{
	public static void Main()
	{
		File<int> fi = new File<int>();
		fi.enfiler(5);
		fi.enfiler(4);
		fi.afficher();
		fi.defiler();
		fi.afficher();

		Console.WriteLine();

		File<Point<int>> fp = new File<Point<int>>();
		fp.enfiler(new Point<int>(1, 2));
		fp.enfiler(new Point<int>(3, 4));
		fp.afficher();
		fp.defiler();
		fp.afficher();
	}
}
