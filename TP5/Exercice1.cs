using System;

public class Point<T> where T : IComparable
{
	private T x;

	private T y;

	public T coordX
	{
		get { return x; }
	}

	public T coordY
	{
		get { return y; }
	}

	public Point(T x, T y)
	{
		this.x = x;
		this.y = y;
	}

	public override string ToString()
	{
		return "Point("+x+","+y+")";
	}
}

public class Exercice1
{
	public delegate int Compare<T>(T a, T b);

	public static void Afficher<T>(T a)
	{
		Console.WriteLine(a);
	}

	public static void Echanger<T>(ref T a, ref T b)
	{
		T tmp = a;
		a = b;
		b = tmp;
	}

	public static IComparable Min(IComparable a, IComparable b)
	{
		return (a.CompareTo(b) < 0) ? a : b;
	}

	public static T Max<T>(T a, T b, Compare<T> f)
	{
		return (f(a, b) > 0) ? a : b;
	}

	public static int compareInt(int a, int b)
	{
		return (a-b);
	}

	public static int comparePoint<T>(Point<T> a, Point<T> b) where T : IComparable
	{
		T ax = a.coordX;
		T bx = b.coordX;
		T ay = a.coordY;
		T by = b.coordY;

		if(ax.CompareTo(bx) == 0 && ay.CompareTo(by) == 0)
			return 0;
		else if(ax.CompareTo(bx) < 0 && ay.CompareTo(by) < 0)
			return -1;
		else
			return 1;
	}

	/*
	static void Main()
	{
		int a = 10;
		int b = 5;

		Point<double> p1 = new Point<double>(12.4, 10.0);
		Point<double> p2 = new Point<double>(0.4, 5.9);

		Console.Write("Max("+a+", "+b+") = ");
		int c = Max(a, b, compareInt);
		Console.WriteLine(c);

		Console.WriteLine("Max("+p1+", "+p2+") = "+Max(p1, p2, comparePoint));
	}
	*/
}
