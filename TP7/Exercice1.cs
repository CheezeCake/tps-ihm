using System;
using System.Windows.Forms;
using System.Drawing;
using System.Globalization;

public class Convertisseur : Form
{
	MenuItem yen;
	MenuItem ls;
	TextBox valeur;
	Label resultat;

	private void menuClick(object sender, EventArgs e)
	{
		if(sender == yen)
		{
			yen.Checked = true;
			ls.Checked = false;
		}
		else
		{
			ls.Checked = true;
			yen.Checked = false;
		}
	}

	private void convertir(object sender, EventArgs e)
	{
		double v;

		try
		{
			v = Double.Parse(valeur.Text, NumberStyles.Float);

			// http://finance.yahoo.com/currency-converter
			if(yen.Checked)
				v *= 141.78;
			else
				v *= 0.82;

			resultat.Text = "Résultat de la conversion: "+v;

		}
		catch(Exception)
		{
			MessageBox.Show("Saisissez une valeur en euros");
		}
	}

	public Convertisseur() : base()
	{
		MainMenu menu = new MainMenu();
		Menu = menu;

		MenuItem monnaie = new MenuItem("Monnaie");
		menu.MenuItems.Add(monnaie);

		yen = new MenuItem("&Yen");
		yen.Click += new EventHandler(menuClick);
		yen.Checked = true;
		monnaie.MenuItems.Add(yen);

		ls = new MenuItem("&Livres Sterling");
		ls.Click += new EventHandler(menuClick);
		monnaie.MenuItems.Add(ls);


		Label label = new Label();
		label.Text = "Valeur à convertir";
		label.Location = new Point(10, 10);
		label.Size = new Size(100, 30);

		valeur = new TextBox();
		valeur.Location = new Point(110, 10);
		valeur.Size = new Size(100, 30);

		resultat = new Label();
		resultat.Location = new Point(10, 60);
		resultat.Size = new Size(200, 30);

		Button bouton = new Button();
		bouton.Text = "Convertir";
		bouton.Location = new Point(75, 80);
		bouton.Size = new Size(70, 40);
		bouton.Click += new EventHandler(convertir);

		Controls.Add(label);
		Controls.Add(valeur);
		Controls.Add(resultat);
		Controls.Add(bouton);
		Size = new Size(230, 170);
	}

	public static void Main()
	{
		Convertisseur c = new Convertisseur();
		c.StartPosition = FormStartPosition.CenterScreen;
		Application.Run(c);
	}
}
