using System;
using System.Windows.Forms;
using System.Drawing;
using System.Globalization;
using System.Collections.Generic;

public class Convertisseur : Form
{
	MenuItem monnaie; // menu cible

	TextBox valeur; // valeur.ContextMenu : menu source
	Label resultat;

	Dictionary<string, Dictionary<string, double>> conversions; //Source, conversionsSource
	Dictionary<string, double> conversionsEuro;
	Dictionary<string, double> conversionsDollar;

	private MenuItem Cible
	{
		get
		{
			MenuItem ret = getCheckedItem(monnaie.MenuItems);

			if(ret != null)
				return ret;

			throw new Exception("Menu monnaie cible sans sélection");
		}

		set
		{
			check(value, monnaie.MenuItems);
		}
	}

	private MenuItem Source
	{
		get
		{
			MenuItem ret = getCheckedItem(valeur.ContextMenu.MenuItems);

			if(ret != null)
				return ret;

			throw new Exception("Menu monnaie source sans sélection");
		}

		set
		{
			check(value, valeur.ContextMenu.MenuItems);
		}
	}

	private MenuItem getCheckedItem(Menu.MenuItemCollection items)
	{
		foreach(MenuItem item in items)
		{
			if(item.Checked)
				return item;
		}

		return null;
	}

	private void check(MenuItem aChecker, Menu.MenuItemCollection items)
	{
		foreach(MenuItem item in items)
			item.Checked = false;

		aChecker.Checked = true;
	}

	private void menuClick(object sender, EventArgs e)
	{
		Cible = (MenuItem)sender;
	}

	private void contextMenuClick(object sender, EventArgs e)
	{
		Source = (MenuItem)sender;
	}

	private void convertir(object sender, EventArgs e)
	{
		double v;

		try
		{
			v = Double.Parse(valeur.Text, NumberStyles.AllowDecimalPoint);
			v *= conversions[Source.Text][Cible.Text];

			resultat.Text = "Résultat de la conversion: "+v;

		}
		catch(Exception)
		{
			MessageBox.Show("Saisissez une valeur en euros");
		}
	}

	private void fenetreAjout(object sender, EventArgs e)
	{
		FormulaireAjout form = new FormulaireAjout();

		form.ShowDialog(this);
		ajouterMonnaie(form.Nom, form.ValEuro, form.ValDollar);
	}

	private void creerChampValeur()
	{
		valeur = new TextBox();
		valeur.Location = new Point(110, 10);
		valeur.Size = new Size(100, 30);

		ContextMenu menu = new ContextMenu();
		valeur.ContextMenu = menu;

		MenuItem contextEuro = new MenuItem("Euro");
		contextEuro.Checked = true;
		contextEuro.Click += new EventHandler(contextMenuClick);
		menu.MenuItems.Add(contextEuro);

		MenuItem contextDollar = new MenuItem("Dollar");
		contextDollar.Click += new EventHandler(contextMenuClick);
		menu.MenuItems.Add(contextDollar);
	}

	private void creerDictionnaires()
	{
		conversions = new Dictionary<string, Dictionary<string, double>>();

		conversionsEuro = new Dictionary<string, double>();
		conversionsDollar = new Dictionary<string, double>();

		conversions.Add("Euro", conversionsEuro);
		conversions.Add("Dollar", conversionsDollar);
	}

	private void creerMainMenu()
	{
		MainMenu menu = new MainMenu();
		Menu = menu;

		monnaie = new MenuItem("Monnaie");
		menu.MenuItems.Add(monnaie);

		MenuItem ajouter = new MenuItem("Ajouter");
		ajouter.Click += new EventHandler(fenetreAjout);
		menu.MenuItems.Add(ajouter);
	}

	private void ajouterMonnaie(string nom, double valEuro, double valDollar, bool check = false)
	{
		MenuItem item = new MenuItem(nom);
		item.Checked = check;
		item.Click += new EventHandler(menuClick);
		monnaie.MenuItems.Add(item);

		conversionsEuro.Add(nom, valEuro);
		conversionsDollar.Add(nom, valDollar);
	}

	public Convertisseur() : base()
	{
		creerDictionnaires();

		creerMainMenu();
		ajouterMonnaie("Yen", 141.78, 102.21, true);
		ajouterMonnaie("Livres Sterling", 0.82, 0.59);

		creerChampValeur();


		Label label = new Label();
		label.Text = "Valeur à convertir";
		label.Location = new Point(10, 10);
		label.Size = new Size(100, 30);

		resultat = new Label();
		resultat.Location = new Point(10, 60);
		resultat.Size = new Size(200, 30);

		Button bouton = new Button();
		bouton.Text = "Convertir";
		bouton.Location = new Point(75, 80);
		bouton.Size = new Size(70, 40);
		bouton.Click += new EventHandler(convertir);

		Controls.Add(label);
		Controls.Add(valeur);
		Controls.Add(resultat);
		Controls.Add(bouton);
		AcceptButton = bouton;
		Size = new Size(230, 170);
	}

	public static void Main()
	{
		Convertisseur c = new Convertisseur();
		c.StartPosition = FormStartPosition.CenterScreen;
		Application.Run(c);
	}
}

public class FormulaireAjout : Form
{
	TextBox nom;
	TextBox valEuro;
	TextBox valDollar;

	public string Nom
	{
		get; set;
	}

	public double ValEuro
	{
		get; set;
	}

	public double ValDollar
	{
		get; set;
	}

	private void close(object sender, EventArgs e)
	{
		try
		{
			ValEuro = Double.Parse(valEuro.Text, NumberStyles.AllowDecimalPoint);
			ValDollar = Double.Parse(valDollar.Text, NumberStyles.AllowDecimalPoint);

			if(nom.Text == "")
				throw new Exception();

			Nom = nom.Text;

			Close();
		}
		catch(Exception)
		{
			MessageBox.Show("Erreur valeurs !");
		}
	}

	public FormulaireAjout() : base()
	{
		Label labelNom =  new Label();
		labelNom.Text = "Nouvelle Monnaie";
		labelNom.Location = new Point(10, 10);
		labelNom.Size = new Size(100, 30);

		nom = new TextBox();
		nom.Location = new Point(120, 10);
		nom.Size = new Size(100, 30);

		Label labelEuro = new Label();
		labelEuro.Text = "Conversion en Euros";
		labelEuro.Location = new Point(10, 50);
		labelEuro.Size = new Size(100, 30);

		valEuro = new TextBox();
		valEuro.Location = new Point(120, 50);
		valEuro.Size = new Size(100, 30);

		Label labelDollar = new Label();
		labelDollar.Text = "Conversion en Dollar";
		labelDollar.Location = new Point(10, 90);
		labelDollar.Size = new Size(100, 30);

		valDollar = new TextBox();
		valDollar.Location = new Point(120, 90);
		valDollar.Size = new Size(100, 30);

		Button bouton = new Button();
		bouton.Text = "Ajouter";
		bouton.Location = new Point(0, 130);
		bouton.Size = new Size(230, 30);
		bouton.Click += new EventHandler(close);

		Controls.Add(labelNom);
		Controls.Add(nom);
		Controls.Add(labelEuro);
		Controls.Add(valEuro);
		Controls.Add(labelDollar);
		Controls.Add(valDollar);
		Controls.Add(bouton);
		AcceptButton = bouton;
		Size = new Size(230, 190);
	}
}

