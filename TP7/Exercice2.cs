using System;
using System.Windows.Forms;
using System.Drawing;
using System.Globalization;
using System.Collections.Generic;

public class Convertisseur : Form
{
	MenuItem yen;
	MenuItem ls;
	TextBox valeur;
	Label resultat;

	Dictionary<string, double> conversions;

	private void menuClick(object sender, EventArgs e)
	{
		if(sender == yen)
		{
			yen.Checked = true;
			ls.Checked = false;
		}
		else
		{
			ls.Checked = true;
			yen.Checked = false;
		}
	}

	private void convertir(object sender, EventArgs e)
	{
		double v;

		try
		{
			v = Double.Parse(valeur.Text, NumberStyles.AllowDecimalPoint);

			if(yen.Checked)
				v *= conversions[yen.Text];
			else
				v *= conversions[ls.Text];

			resultat.Text = "Résultat de la conversion: "+v;

		}
		catch(Exception)
		{
			MessageBox.Show("Saisissez une valeur en euros");
		}
	}

	public Convertisseur() : base()
	{
		conversions = new Dictionary<string, double>();


		MainMenu menu = new MainMenu();
		Menu = menu;

		MenuItem monnaie = new MenuItem("Monnaie");
		menu.MenuItems.Add(monnaie);

		yen = new MenuItem("Yen");
		yen.Click += new EventHandler(menuClick);
		yen.Checked = true;
		monnaie.MenuItems.Add(yen);
		conversions.Add("Yen", 141.78);

		ls = new MenuItem("Livres Sterling");
		ls.Click += new EventHandler(menuClick);
		monnaie.MenuItems.Add(ls);
		conversions.Add("Livres Sterling", 0.82);


		Label label = new Label();
		label.Text = "Valeur à convertir";
		label.Location = new Point(10, 10);
		label.Size = new Size(100, 30);

		valeur = new TextBox();
		valeur.Location = new Point(110, 10);
		valeur.Size = new Size(100, 30);

		resultat = new Label();
		resultat.Location = new Point(10, 60);
		resultat.Size = new Size(200, 30);

		Button bouton = new Button();
		bouton.Text = "Convertir";
		bouton.Location = new Point(75, 80);
		bouton.Size = new Size(70, 40);
		bouton.Click += new EventHandler(convertir);

		Controls.Add(label);
		Controls.Add(valeur);
		Controls.Add(resultat);
		Controls.Add(bouton);
		AcceptButton = bouton;
		Size = new Size(230, 170);
	}

	public static void Main()
	{
		Convertisseur c = new Convertisseur();
		c.StartPosition = FormStartPosition.CenterScreen;
		Application.Run(c);
	}
}
