using System;
using System.Windows.Forms;
using System.Drawing;
using System.Globalization;
using System.Collections.Generic;

public class Convertisseur : Form
{
	MenuItem yen;
	MenuItem ls;
	TextBox valeur;
	Label resultat;
	MenuItem contextEuro;
	MenuItem contextDollar;

	Dictionary<string, Dictionary<string, double>> conversions; //Source, conversionsSource
	Dictionary<string, double> conversionsEuro;
	Dictionary<string, double> conversionsDollar;

	private void menuClick(object sender, EventArgs e)
	{
		if(sender == yen)
		{
			yen.Checked = true;
			ls.Checked = false;
		}
		else
		{
			ls.Checked = true;
			yen.Checked = false;
		}
	}

	private void contextMenuClick(object sender, EventArgs e)
	{
		if(sender == contextEuro)
		{
			contextEuro.Checked = true;
			contextDollar.Checked = false;
		}
		else
		{
			contextDollar.Checked = true;
			contextEuro.Checked = false;
		}
	}

	private void convertir(object sender, EventArgs e)
	{
		double v;

		try
		{
			v = Double.Parse(valeur.Text, NumberStyles.AllowDecimalPoint);

			string source = (contextEuro.Checked) ? contextEuro.Text : contextDollar.Text;

			if(yen.Checked)
				v *= conversions[source][yen.Text];
			else
				v *= conversions[source][ls.Text];

			resultat.Text = "Résultat de la conversion: "+v;

		}
		catch(Exception)
		{
			MessageBox.Show("Saisissez une valeur en euros");
		}
	}

	private void creerChampValeur()
	{
		valeur = new TextBox();
		valeur.Location = new Point(110, 10);
		valeur.Size = new Size(100, 30);

		ContextMenu menu = new ContextMenu();
		valeur.ContextMenu = menu;

		contextEuro = new MenuItem("Euro");
		contextEuro.Checked = true;
		contextEuro.Click += new EventHandler(contextMenuClick);
		menu.MenuItems.Add(contextEuro);

		contextDollar = new MenuItem("Dollar");
		contextDollar.Click += new EventHandler(contextMenuClick);
		menu.MenuItems.Add(contextDollar);
	}

	public Convertisseur() : base()
	{
		conversions = new Dictionary<string, Dictionary<string, double>>();

		conversionsEuro = new Dictionary<string, double>();
		conversionsDollar = new Dictionary<string, double>();

		conversions.Add("Euro", conversionsEuro);
		conversions.Add("Dollar", conversionsDollar);


		MainMenu menu = new MainMenu();
		Menu = menu;

		MenuItem monnaie = new MenuItem("Monnaie");
		menu.MenuItems.Add(monnaie);

		yen = new MenuItem("Yen");
		yen.Click += new EventHandler(menuClick);
		yen.Checked = true;
		monnaie.MenuItems.Add(yen);
		conversionsEuro.Add("Yen", 141.78);
		conversionsDollar.Add("Yen", 102.21);

		ls = new MenuItem("Livres Sterling");
		ls.Click += new EventHandler(menuClick);
		monnaie.MenuItems.Add(ls);
		conversionsEuro.Add("Livres Sterling", 0.82);
		conversionsDollar.Add("Livres Sterling", 0.59);


		creerChampValeur();


		Label label = new Label();
		label.Text = "Valeur à convertir";
		label.Location = new Point(10, 10);
		label.Size = new Size(100, 30);

		resultat = new Label();
		resultat.Location = new Point(10, 60);
		resultat.Size = new Size(200, 30);

		Button bouton = new Button();
		bouton.Text = "Convertir";
		bouton.Location = new Point(75, 80);
		bouton.Size = new Size(70, 40);
		bouton.Click += new EventHandler(convertir);

		Controls.Add(label);
		Controls.Add(valeur);
		Controls.Add(resultat);
		Controls.Add(bouton);
		AcceptButton = bouton;
		Size = new Size(230, 170);
	}

	public static void Main()
	{
		Convertisseur c = new Convertisseur();
		c.StartPosition = FormStartPosition.CenterScreen;
		Application.Run(c);
	}
}
