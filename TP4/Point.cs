using System;

public class Point
{
	private int x;

	private int y;

	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public override string ToString()
	{
		return "x = "+x+", y = "+y;
	}

	public bool XInf(int x) { return (this.x < x); }

	public bool YInf(int y) { return (this.y < y); }

	public bool XSup(int x) { return (this.x > x); }

	public bool YSup(int y) { return (this.y > y); }

	public delegate bool Comparaison(int a);

	public void afficherSiVrai(Comparaison f, int a)
	{
		if(f(a))
			Console.WriteLine(this);
	}
}

public class Test
{
	public static void afficherTab(Point[] p, Point.Comparaison f, int a)
	{
		for(int i = 0; i < p.Length; i++)
			p[i].afficherSiVrai(f, a);
	}

	public static void Main()
	{
		Point[] p = new Point[3];
		p[0] = new Point(5, 9);
		p[1] = new Point(6, 4);
		p[2] = new Point(0, 1);

		afficherTab(p, p[0].XInf, 2);
	}
}
