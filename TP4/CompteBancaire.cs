using System;

public class CompteEpargne
{
	protected int solde;

	public CompteEpargne()
	{
		this.solde = 100;
	}

	public bool debit(int montant)
	{
		if(solde < montant)
			return false;

		solde -= montant;
		return true;
	}
}

public class CompteBancaire
{
	protected int solde;

	protected CompteEpargne ce;

	public delegate void delegateDecouvert();

	public event delegateDecouvert decouvert;

	public CompteBancaire(int solde)
	{
		this.solde = solde;
		ce = new CompteEpargne();
	}

	public void credit(int v)
	{
		solde += v;
	}

	public void debit(int v)
	{
		solde -= v;

		if(solde < 0)
		{
			if(decouvert != null)
				decouvert();

			if(ce.debit(-solde))
				solde = 0;
			else
				throw new Exception("Epargne insuffisante");
		}
	}
}

public class Test
{
	public static void compteADecouvert()
	{
		Console.WriteLine("Compte a decouvert");
	}

	public static void Main()
	{
		CompteBancaire c = new CompteBancaire(100);

		c.decouvert += compteADecouvert;

		c.debit(400);
	}
}
