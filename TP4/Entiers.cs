using System;

public delegate bool Comparaison(int a, int b);

public class Entiers
{
	public static bool Superieur(int a, int b) { return (a > b); }

	public static bool Inferieur(int a, int b) { return (a < b); }

	public static void Main()
	{
		int[] tab = new int[] {33, 42, 10, 8, -4, 7};
		Comparaison c;
		int v = 30;
		int choix;

		Console.Write("afficher les entiers:\n (1) superieurs a {0}\n (2) inferieurs a {0}\n> ", v);
		choix = int.Parse(Console.ReadLine());

		if(choix == 1)
			c = Superieur;
		else
			c = Inferieur;

		for(int i = 0; i < tab.Length; i++)
		{
			if(c(tab[i], v))
				Console.WriteLine(tab[i]);
		}
	}
}
