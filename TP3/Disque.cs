using System;

public class Disque
{
	public Point centre;

	private int rayon;

	public Point Centre
	{
		get { return new Point(centre.X, centre.Y); }
	}

	private Disque hist;

	public Disque(int x, int y, int rayon)
	{
		centre = new Point(x, y);
		this.rayon = rayon;
		hist = null;
	}

	public Disque(Point centre, int rayon)
	{
		this.centre = new Point(centre.X, centre.Y);
		hist = null;
	}

	public override string ToString()
	{
		return "Centre: "+centre.ToString()+", rayon: "+rayon;
	}

	public void sauv()
	{
		//hist = new Point(x, y);
		hist = (Disque)Clone();
	}

	public Disque Clone()
	{
		return new Disque(centre.X, centre.Y, rayon);
	}

	public void restaure()
	{
		if(hist == null)
			throw new Exception("Aucune valeur savegardée à restaurer");

		centre = hist.Centre;
		rayon = hist.rayon;
	}

	public static void Main()
	{
		Disque d = new Disque(1, 2, 10);
		Console.WriteLine(d);

		d.sauv();

		Point p = new Point(3, 4);
		d.centre = p;
		Console.WriteLine(d);

		d.restaure();
		Console.WriteLine(d);
	}
}
