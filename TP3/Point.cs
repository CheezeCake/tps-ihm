using System;

public class Point
{
	private int x;

	private int y;

	public int X
	{
		get { return x; }
		
		set { x = value; }
	}

	public int Y
	{
		get { return y; }
		
		set { y = value; }
	}

	private Point hist;

	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
		hist = null;
	}

	public override string ToString()
	{
		return "x = "+x+", y = "+y;
	}

	public void sauv()
	{
		//hist = new Point(x, y);
		hist = (Point)MemberwiseClone();
	}

	public void restaure()
	{
		if(hist == null)
			throw new Exception("Aucune valeur savegardée à restaurer");

		x = hist.x;
		y = hist.y;
	}

	/*
	public static void Main()
	{
		Point p = new Point(2, 3);
		p.sauv();
		p.X = 5;
		Console.WriteLine(p);
		p.restaure();
		Console.WriteLine(p);
	}
	*/
}
