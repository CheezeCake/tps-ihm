using System;

public static class Historique
{
	public static int[] valeurs = new int[5];

	public static int indiceDerniere = -1;

	public static int derniereValeur()
	{
		if(indiceDerniere == -1)
			throw new Exception("Aucune valeur savegardée");

		return valeurs[indiceDerniere];
	}

	public static void sauvegarder(int v)
	{
		if(indiceDerniere == valeurs.Length-1)
			throw new Exception("Tableau plein");

		valeurs[++indiceDerniere] = v;
	}
}
