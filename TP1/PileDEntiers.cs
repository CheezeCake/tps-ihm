using System;

public class PileDEntiers
{
	private int[] pile;
	private int tete = -1;

	public PileDEntiers()
	{
		pile = new int[20];
	}

	public PileDEntiers(int n)
	{
		pile = new int[n];
	}

	public void empile(int p)
	{
		if(pleine())
			throw new Exception("Pile pleine");
		else
			pile[++tete] = p;
	}

	public int depile()
	{
		if(vide())
			throw new Exception("Pile vide");
		else
			return pile[tete--];
	}

	public bool pleine()
	{
		return (tete == (pile.Length-1));
	}

	public bool vide()
	{
		return (tete == -1);
	}

	public void affiche()
	{
		for(int i = 0; i <= tete; i++)
			Console.Write(pile[i]+" ");
		Console.WriteLine();
	}

	public int nbElements()
	{
		return (tete+1);
	}

	public static PileDEntiers operator+(PileDEntiers p, int n)
	{
		for(int i = 0; i < p.nbElements(); i++)
			p.pile[i] += n;

		return p;
	}

	public static void Main()
	{
		Console.WriteLine("test");
	}
}
