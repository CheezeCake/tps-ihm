using System;

public static class Extension
{
	public static void afficher(this Point3D p)
	{
		Console.WriteLine(p.toString());
	}
}

public class Point3D
{
	private int x;

	private int y;

	private int z;

	public String toString()
	{
		return "x = "+x+", y = "+y+", z = "+z;
	}

	public int this [char index]
	{
		get
		{
			switch(index)
			{
				case 'x':
					return x;
				case 'y':
					return y;
				case 'z':
					return z;
				default:
					throw new Exception("Index '"+index+"' inconnu");
			}
		}

		set
		{
			switch(index)
			{
				case 'x':
					x = value;
					break;
				case 'y':
					y = value;
					break;
				case 'z':
					z = value;
					break;
				default:
					throw new Exception("Index '"+index+"' inconnu");
			}
		}
	}

	public void affecter(Point3D p)
	{
		x = p.x;
		y = p.y;
		z = p.z;
	}

	public static void Main()
	{
		Point3D p = new Point3D();

		Console.Write("x: ");
		p['x'] = int.Parse(Console.ReadLine());
		Console.Write("y: ");
		p['y'] = int.Parse(Console.ReadLine());
		Console.Write("z: ");
		p['z'] = int.Parse(Console.ReadLine());

		Console.WriteLine(p.toString());
		p.afficher();
	}
}
